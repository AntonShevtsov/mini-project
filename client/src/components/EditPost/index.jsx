import React from 'react';
import { Form, Button, Segment } from 'semantic-ui-react';

class EditPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            post: this.props.post,
        };
    }
    
    handleEditPost = async () => {
        const  { post }  = this.state;
        if (!post.body) {
            this.props.editPost(this.props.post);
            return;
        }
        await this.props.updatePost(post);
        this.props.editPost(this.props.post);
    }

    render() {
        return (
            <Segment>
                <Form onSubmit={this.handleEditPost}>
                    <Form.TextArea
                        name="body"       
                        value={this.state.post.body}
                        onChange={ev => { 
                            const post = {...this.state.post};
                            post.body = ev.target.value;                     
                            this.setState({post})}                   
                        }
                    />
                    <Button floated="right" color="blue" type="submit">Edit</Button>
                </Form>
            </Segment>
        );
    }
}

export default EditPost;
