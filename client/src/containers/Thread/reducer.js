import {
    SET_ALL_POSTS,
    LOAD_MORE_POSTS,
    ADD_POST,
    SET_EXPANDED_POST,
    DELETE_POST,
    EDIT_POST,
    UPDATE_POST,
} from './actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_POSTS:
            return {
                ...state,
                posts: action.posts,
                hasMorePosts: Boolean(action.posts.length)
            };
        case LOAD_MORE_POSTS:
            return {
                ...state,
                posts: [...(state.posts || []), ...action.posts],
                hasMorePosts: Boolean(action.posts.length)
            };
        case EDIT_POST:
            const updatedPosts = state.posts.map(post => {
                if(post.id === action.post.id){
                    return { ...action.post }
                }
                return post
            })
            return {
                ...state,
                posts: [...updatedPosts]
            };
        case UPDATE_POST:
            return {
                ...state,
                posts: [
                    ...state.posts.slice(0, state.posts.findIndex(post => post.id === action.post.id)),
                    action.post,
                    ...state.posts.slice(state.posts.findIndex(post => post.id === action.post.id) + 1)
                ]
            };
        case ADD_POST:
            return {
                ...state,
                posts: [action.post, ...state.posts]
            };
        case DELETE_POST:
            return {
                ...state,
                posts: [...state.posts.slice(0, state.posts.findIndex(post => post.id === action.post.id)),
                        ...state.posts.slice(state.posts.findIndex(post => post.id === action.post.id) + 1)]
            };
        case SET_EXPANDED_POST:
            return {
                ...state,
                expandedPost: action.post
            };
        default:
            return state;
    }
};
